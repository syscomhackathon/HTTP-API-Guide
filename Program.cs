﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using Newtonsoft.Json;

namespace HTTP_API_Guide
{
    class Program
    {
        static void Main(string[] args)
        {

            //==================== HTTP i C# ====================//

            // For å kunne gjøre http kall må man instansiere opp ett objekt av klassen HttpClient
            HttpClient client0 = new HttpClient();

            // HttpClient benytter seg av en spesiell teknikk som kalles for asynkrone funksjonskall.
            // Dette går vi ikke inn på men det betyr at man må legge til .Result på slutten av et kall for at funksjonen skal kjøre 
            // Med client objektet kan vi nå gjøre kall mot en webserver feks:
            var result0 = client0.GetAsync("http://www.syscomworld.com").Result;
            
            // skrive resultat til console 
            // (Merk: en webside som syscomworld.com inneholder veldig masse tekst. Man får all html-kode og vanlig tekst på siden):
            Console.WriteLine(result0.Content.ReadAsStringAsync().Result);
            // MERK: "result.Content.ReadAsStringAsync().Result" er egentlig bare mekanikk for å få hentet resultatet 
            // av kallet og har som sådan ikke noe med spørringen å gjøre. MAO: ikke tenk på det :)
            
            // Vent litt og tøm konsollet
            Thread.Sleep(3000);
            Console.Clear();

            
            //==================== Pureservice ====================//

            // For å kunne gjøre spørringer mot Pureservice sitt API krever det at vi hopper gjennom noen flere ringer.
            
            // 0. Lage ny HttpClient
            HttpClient client1 = new HttpClient(); 
            
            // 1. For å slippe å tenke på hvilke server man skal gå mot kan vi sette BaseAddress property på client1 objektet vårt.
            // Da betyr det at client1 vil alltid peke på adressen vi spesifiserer.
            // Her må vi også benytte oss av en ny klasse Uri, som egentlig bare sørger for å validere at uri er gyldig formatert:
            client1.BaseAddress = new Uri("https://canary.pureservice.com");

            // 2. Vi må ha to headere i kallet vårt, en som heter Accept og en som heter X-Authorization-key
                // a. Accept spesifiserer hvordan data vi vil ha tilbake, hvilket format de skal være på.
                // I  Pureservice må det stå "application/vnd.api+json" der
            client1.DefaultRequestHeaders.Add("Accept", "application/vnd.api+json");

                // b. X-Authorization-Key er nøkkelen som gir oss tilgang. Denne må lages og kopieres fra Pureservice Administratoren
            client1.DefaultRequestHeaders.Add("X-Authorization-Key","en-ikke-gyldig-token:eyJ0U4Qt4Ty1cvo8sU-lag-en-ny-i-pureservice-admin");


            // 3. hent ut ticket med id 1. Full uri for det er: https://canary.pureservice.com/agent/api/ticket/1/ men siden vi allerede
            // har spesifisert "base url" så trenger vi bare angi den delen som kalles for "query path"
            var result1 = client1.GetAsync("agent/api/ticket/1/").Result;
            
            // 4. sjekke for feil
            if(!result1.IsSuccessStatusCode)
            {
                Console.Clear();
                Console.WriteLine("Spørring mot Pureservice API feilet med: "+ result1.ReasonPhrase);
                return; //avslutte om noe er feil
            }
 
            // skrive resultat til console (dette vil være json-formatert data):
            Console.WriteLine(result1.Content.ReadAsStringAsync().Result);
            // hvis alt er satt opp korrekt vil man få json data som ser omtrent ut som her:
            // https://gitlab.com/syscomhackathon/HTTP-API-Guide/snippets/1680813
            

            // Vent litt og tøm konsollet
            Thread.Sleep(3000);
            Console.Clear();


            ContinueToNextLesson(); // Les videre nedover så kommer man etterhvert til denne funksjonen som er neste kode-leksjon.
        }

        //==================== JSON deserialisering ====================//

        // For å kunne ta dataene fra svaret vi får fra API over til et format som er enklere å jobbe med i C#
        // benytter vi oss av en JSON-deserialiserer. For å kunne gjøre dette må man først spesifisere 
        // strukturen på datene man vil deserialisere.
        // JSON er rimelig enkelt strukturert tegnet "{" (start klamme) signaliserer starten på et objekt,
        // mens tegnet "[" (start array) signaliserer starten på en liste.


        // For å kunne få ut Id verdien fra en ticket må vi lage to klasser for å duplisere json-strukturen
        // som kommer fra serveren

        // Den ytterste klammen i json-svaret kaller vi for APIResponse og den inneholder en "tickets" liste eller array
        class APIResponse
        {
            // Klasse konstruktør. Denne funksjonen kjøres automatisk når man lager et nytt objekt av klassen 
            // eks: APIResponse r = new APIResponse();
            public APIResponse()
            {
                //Vi initialiserer listen til en tom liste når klassen blir instansiert.
                this.Tickets = new List<Ticket>();
            }
            
            // Vi legger på en attributten JsonProperty for å signalisere hva dette elementet heter i json strukturen
            [JsonProperty("tickets")]
            public List<Ticket> Tickets {get; set;} 
        }

        // En ticket i tickets listen inneholder mange verdier men vi skal bare se på id, requestNumber (saksnummeret) og subject (emne)
        // Vi trenger kun å definere de feltene vi er interressert i
        class Ticket 
        {
            [JsonProperty("id")]
            public int Id {get; set;}
            
            [JsonProperty("requestNumber")]
            public int TicketNumber {get; set;}
            
            //Merk at subject (emne) er string
            [JsonProperty("subject")]
            public string Emne {get; set;}
        }

        public static void ContinueToNextLesson()
        {
            // Ved å benytte HttpClient og leksjonen som er lenger opp, henter vi ut en ticket json struktur:

            HttpClient client2 = new HttpClient(); 
            
            client2.BaseAddress = new Uri("https://canary.pureservice.com");
            client2.DefaultRequestHeaders.Add("Accept", "application/vnd.api+json");
            client2.DefaultRequestHeaders.Add("X-Authorization-Key","man-må-opprette-og-legge-inn-sin-api-token-her");
            var result2 = client2.GetAsync("agent/api/ticket/1/").Result;
            
            if(!result2.IsSuccessStatusCode)
            {
                Console.Clear();
                Console.WriteLine("Spørring mot Pureservice API feilet med: "+ result2.ReasonPhrase);
                return; //avslutte om noe er feil
            }

            // Nå kan vi deserialisere json-strukturen inn i de forhåndsdefinerte klassene våre:
            // Da benytter vi et eksternt bibliotek som heter Newtonsoft.Json (command: dotnet add package Newtonsoft.Json)
            APIResponse response = JsonConvert.DeserializeObject<APIResponse>(result2.Content.ReadAsStringAsync().Result);

            // Navigere inn i strukturen ved å bruke punktudom
            Console.WriteLine("Ticket Id: "+ response.Tickets.First().Id);
            Console.WriteLine("Ticket Nummer: "+ response.Tickets.First().TicketNumber);
            Console.WriteLine("Ticket Emne: "+ response.Tickets.First().Emne);  
        }
        
    }
}
